from django.shortcuts import render
from datetime import datetime
from uuid import uuid4

from django.contrib.auth import authenticate
from django.contrib.auth import login as dj_login
from django.contrib.auth import logout as dj_logout
from django.contrib.auth.models import Group
from django.contrib.auth.models import User as Users
from django.db import IntegrityError, transaction
from django.utils.decorators import method_decorator
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView

from generic.misc.decorators import SessionAuthentication, TokenAuthentication, check_permission
from generic.response.http import StandardHttpResponse


# Create your views here.
class Login(APIView):

    @staticmethod
    def _required_fields():
        return ["username", "password"]

    def _validate_request_data(self, data):
        if type(data) != dict:
            return False

        for field in self._required_fields():
            if field not in data:
                return False

        return True

    @staticmethod
    def _generate_token(user):
        Token.objects.filter(user=user).delete()
        return Token.objects.create(user=user)

    def post(self, request):
        data = request.data.copy()
        if not self._validate_request_data(data):
            return StandardHttpResponse.bad_rsp([], 'Not all the required fields are present in data')

        user = authenticate(
            username=data['username'], password=data['password'])
        if not user:
            return StandardHttpResponse.rsp_403([], 'The email or password is invalid')

        dj_logout(request)
        dj_login(request, user)
        token = self._generate_token(user)
        headers = {"auth": token.key}
        data = {}
        return StandardHttpResponse.rsp_200(data, "Logged in", headers)
