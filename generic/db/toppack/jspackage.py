from toppack.models import JSPackages
from django.db import connection

__all__ = ('JSPackageModelQueries',)


class JSPackageModelQueries(object):

    @staticmethod
    def get_by_name(name):
        try:
            return JSPackages.objects.get(name=name)
        except JSPackages.DoesNotExist:
            return None

    @staticmethod
    def create_package(name):
        try:
            return JSPackages.objects.create(name=name)
        except Exception as E:
            print("Error occurred {} {}".format(str(E), name))
            return None

    @staticmethod
    def get_top_10_package():
        query = "select (select c.name as name from toppack_jspackages c where id = b.jspackages_id), " \
                "b.jspackages_id, count(*) as count from toppack_package_packages b group by jspackages_id order " \
                "by count desc limit 10;"

        with connection.cursor() as cursor:
            cursor.execute(query)
            row = cursor.fetchall()
        return row

    @staticmethod
    def search_top_3_package(name):
        query = "select (select c.name as name from toppack_jspackages c where id = b.jspackages_id), b.jspackages_id, " \
                "count(*) as count from toppack_package_packages b where b.jspackages_id in " \
                "(select x.id from toppack_jspackages x where x.name ilike '%{}%') " \
                "group by jspackages_id order by count desc limit 3;".format(name)

        with connection.cursor() as cursor:
            cursor.execute(query)
            row = cursor.fetchall()
        return row
