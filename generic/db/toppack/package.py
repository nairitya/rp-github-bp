from toppack.models import Package


class PackageModelQueries(object):

    @staticmethod
    def get_by_id(p_id):
        try:
            return Package.objects.get(github_id=p_id)
        except Package.DoesNotExist:
            return None

    @staticmethod
    def create_package(**data):
        try:
            return Package.objects.create(**data)
        except Exception as E:
            print(str(E))
            return None

    @staticmethod
    def get_details_for_refresh():
        return Package.objects.filter().values_list('github_id', 'id', 'owner', 'name')
