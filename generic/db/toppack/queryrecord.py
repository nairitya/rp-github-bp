from toppack.models import QueryRecord


class QueryRecordModelQueries(object):

    @staticmethod
    def get_by_name(name):
        try:
            return QueryRecord.objects.get(query=name)
        except QueryRecord.DoesNotExist:
            return None

    @staticmethod
    def create_by_name(name):
        return QueryRecord.objects.create(query=name)
