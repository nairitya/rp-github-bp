# Generated by Django 2.2.1 on 2019-05-10 18:07

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GenericLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app', models.CharField(max_length=50)),
                ('data', models.TextField()),
                ('write_time', models.DateTimeField(default=django.utils.timezone.now)),
                ('level', models.CharField(default='debug', max_length=25)),
                ('user', models.UUIDField(blank=True, default=None, null=True)),
                ('env', models.CharField(default='uat', max_length=25)),
            ],
        ),
    ]
