from rest_framework import status
from rest_framework.response import Response

from generic.response import GenericMessageResponse


class StandardHttpResponse(object):
    @staticmethod
    def bad_rsp(obj=list(), message=''):
        return Response(GenericMessageResponse.error_response(obj, message), status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def rsp_403(obj=list(), message=''):
        return Response(GenericMessageResponse.error_response(obj, message), status=status.HTTP_403_FORBIDDEN)

    @staticmethod
    def rsp_401(obj=list(), message=''):
        return Response(GenericMessageResponse.error_response(obj, message), status=status.HTTP_401_UNAUTHORIZED)

    @staticmethod
    def rsp_200(obj=list(), message='', headers=None):
        return Response(GenericMessageResponse.success_response(obj, message), status=status.HTTP_200_OK,
                        headers=headers)

    @staticmethod
    def rsp_200_no_header(obj=list(), message=''):
        return Response(GenericMessageResponse.success_response(obj, message), status=status.HTTP_200_OK)

    @staticmethod
    def rsp_201(obj=list(), message=''):
        return Response(GenericMessageResponse.success_response(obj, message), status=status.HTTP_201_CREATED)

    @staticmethod
    def rsp_202(obj=list(), message=''):
        return Response(GenericMessageResponse.success_response(obj, message), status=status.HTTP_202_ACCEPTED)

    @staticmethod
    def rsp_404(obj=list(), message=''):
        return Response(GenericMessageResponse.success_response(obj, message), status=status.HTTP_404_NOT_FOUND)
