__all__ = ['GenericMessageResponse']


class GenericMessageResponse(object):

    @staticmethod
    def error_response(err, message):
        return {'error': err, 'details': message, 'success': False}

    @staticmethod
    def success_response(success, message):
        return {'success': success, 'message': message, 'error': False}
