import logging

from generic.logs.models import GenericLog
from garden.settings import DEFAULT_ENV, USE_RDS_LOGGING


class LogWriter(object):

    def __init__(self, app):
        self.app = app
        self.use_rds = True
        self.logger = None

        if not USE_RDS_LOGGING:
            self.use_rds = False
            self.import_non_rds_settings()

    def import_non_rds_settings(self):
        self.logger = logging.getLogger(self.app)

    def critical(self, data, **kwargs):
        if self.use_rds:
            return self.write_log(data, 'critical', kwargs.get('user', None), kwargs.get('env', DEFAULT_ENV))
        else:
            return self.logger.critical(data)

    def info(self, data, **kwargs):
        if self.use_rds:
            return self.write_log(data, 'info', kwargs.get('user', None), kwargs.get('env', DEFAULT_ENV))
        else:
            return self.logger.info(data)

    def debug(self, data, **kwargs):
        if self.use_rds:
            return self.write_log(data, 'debug', kwargs.get('user', None), kwargs.get('env', DEFAULT_ENV))
        else:
            return self.logger.debug(data)

    def error(self, data, **kwargs):
        if self.use_rds:
            return self.write_log(data, 'error', kwargs.get('user', None), kwargs.get('env', DEFAULT_ENV))
        else:
            return self.logger.error(data)

    def write_log(self, data, level, user, env):
        return GenericLog.objects.create(
            app=self.app,
            data=data,
            level=level,
            user=user.__str__() if user else user,
            env=env
        )
