from generic.logs.writer import LogWriter

__all__ = ['Logger']


class Logger(object):

    @staticmethod
    def users_logger():
        return LogWriter('users')

    @staticmethod
    def kyc_doc_logger():
        return LogWriter('kyc_doc')

    @staticmethod
    def error_logger():
        return LogWriter('errors')

    @staticmethod
    def case_logger():
        return LogWriter('case')

    @staticmethod
    def refresh_logger():
        return LogWriter('refresh')
