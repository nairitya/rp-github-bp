from django.db import models
from django.utils import timezone


class GenericLog(models.Model):

    app = models.CharField(max_length=50)
    data = models.TextField()
    write_time = models.DateTimeField(default=timezone.now)
    level = models.CharField(max_length=25, default='debug')
    user = models.UUIDField(default=None, null=True, blank=True)
    env = models.CharField(default='uat', max_length=25)

    class Meta:
        app_label = 'generic'
