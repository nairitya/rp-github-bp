from datetime import datetime, timedelta

from rest_framework.views import APIView
from django.utils import timezone
from toppack.github.search import SearchByKeyword
from toppack.github.repository import GetRepositoryDetails, GetFileContent
from toppack.serializers import PackageSerializer

from generic.db.toppack.package import PackageModelQueries
from generic.db.toppack.jspackage import JSPackageModelQueries
from generic.db.toppack.queryrecord import QueryRecordModelQueries

from generic.response.http import StandardHttpResponse
# Create your views here.

__all__ = ('GithubSearchByKeyword', 'ImportRepository', 'TopTenJSPackage', 'JSPackageSearch')


class GithubSearchByKeyword(APIView):

    def post(self, request):
        pass

    def get(self, request):
        key_word = request.GET.get('q', None)
        page = request.GET.get('page', 1)
        enable = request.GET.get('mark_if_import', "no")

        if not key_word:
            return StandardHttpResponse.bad_rsp([], "Missing the keyword, try again")

        last_query = QueryRecordModelQueries.get_by_name(key_word)
        if last_query:
            current_time = timezone.now()
            minute_diff = current_time - last_query.created_at
            if minute_diff.seconds < 60:
                return StandardHttpResponse.bad_rsp([], "Cannot do same query search in one min, try again later")
            last_query.created_at = timezone.now()
            last_query.save()
        else:
            QueryRecordModelQueries.create_by_name(key_word)

        key_word += '+language:javascript'
        res_bool, response = SearchByKeyword.get_results(key_word, page)

        if not res_bool:
            return StandardHttpResponse.bad_rsp(response, "Failed to search in Github")

        if enable == "yes":
            for x in response["items"]:
                obj = PackageModelQueries.get_by_id(x['id'])
                if obj:
                    x['imported'] = True
                else:
                    x['imported'] = False

        return StandardHttpResponse.rsp_200_no_header(response, "Search data has been brought")


class ImportRepository(APIView):

    def get(self, request):
        owner = request.GET.get('owner', None)
        repo = request.GET.get('repo', None)

        if not owner or not repo:
            return StandardHttpResponse.bad_rsp([], "Missing the required data")

        rep_bool, details = GetRepositoryDetails.get_details(owner, repo)
        if not rep_bool:
            return StandardHttpResponse.bad_rsp([], details)

        obj = PackageModelQueries.get_by_id(details['id'])
        if not obj:
            details['github_id'] = details['id']
            serializer = PackageSerializer(data=details)
            if not serializer.is_valid():
                print(serializer.errors)
                return StandardHttpResponse.bad_rsp(serializer.errors, "Some error occurred, please try again later")
            obj = serializer.save()
        else:
            serializer = PackageSerializer(instance=obj, data=details, partial=True)
            if not serializer.is_valid():
                print(serializer.errors)
                pass
            else:
                obj = serializer.save()

        file_bool, file_content = GetFileContent.get_content(owner, repo)
        if not file_bool:
            return StandardHttpResponse.bad_rsp([], file_content)

        packages = []
        package_to_iterate = dict()

        if 'devDependencies' in file_content:
            package_to_iterate = file_content["devDependencies"]
        if 'dependencies' in file_content:
            package_to_iterate = {**package_to_iterate, **(file_content['dependencies'])}

        for package in package_to_iterate:
            package_obj = JSPackageModelQueries.get_by_name(package)
            if not package_obj:
                package_obj = JSPackageModelQueries.create_package(package)
                if not package_obj:
                    continue
            packages.append(package_obj)

        obj.packages.set(packages)
        return StandardHttpResponse.rsp_200_no_header([], "Repository has been imported")


class TopTenJSPackage(APIView):

    def get(self, request):
        response = JSPackageModelQueries.get_top_10_package()
        return StandardHttpResponse.rsp_200_no_header(response, "Top 10 js packages have been brought")


class JSPackageSearch(APIView):

    def get(self, request):

        query = request.GET.get('query', None)
        if not query:
            return StandardHttpResponse.bad_rsp([], "Missing the required data")

        response = JSPackageModelQueries.search_top_3_package(query)
        return StandardHttpResponse.rsp_200_no_header(response, "Search has completed")
