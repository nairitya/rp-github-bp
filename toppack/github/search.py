import requests
import json

from generic.logs.loggers import Logger
from garden.settings import GITHUB_SEARCH_URL

__all__ = ('SearchByKeyword',)


error_logs = Logger.error_logger()


class SearchByKeyword(object):

    @staticmethod
    def get_results(query, page=1):
        try:
            response = requests.get(GITHUB_SEARCH_URL, params={"q": query, "page": page})
        except Exception as E:
            error_logs.error(str(E))
            return False, E

        return True, json.loads(response.text)
