import requests
import json

from garden.settings import HOME_URL, GITHUB_GET_CONTENT_URL, GITHUB_GET_REPOSITORY_DETAIL
from generic.logs.loggers import Logger

error_logs = Logger.error_logger()

__all__ = ('GetFileContent', 'GetRepositoryDetails')


class GetRepositoryDetails(object):

    @staticmethod
    def get_details(owner, repo):
        try:
            url = GITHUB_GET_REPOSITORY_DETAIL.format(HOME_URL, owner, repo)
            response = requests.get(url)
        except Exception as E:
            error_logs.error(str(E))
            return False, str(E)

        if not response.status_code == 200:
            return False, response.text

        return True, json.loads(response.text)


class GetFileContent(object):

    @staticmethod
    def get_content(owner, repo):
        url = GITHUB_GET_CONTENT_URL.format(HOME_URL, owner, repo)

        try:
            response = requests.get(url)
        except Exception as E:
            error_logs.error(str(E))
            return False, str(E)

        if response.status_code == 404:
            return False, "This project does not contain a package.json file"

        parsed_response = json.loads(response.text)
        download_url = parsed_response['download_url']

        try:
            file_res = requests.get(download_url)
            parsed = json.loads(file_res.text)
        except Exception as E:
            error_logs.error(str(E))
            return False, "Unable to read file, {}".format(str(E))

        return True, parsed
