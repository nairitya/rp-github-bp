from django.contrib import admin
from toppack.models import Package, JSPackages, QueryRecord
# Register your models here.

admin.site.register(Package)
admin.site.register(JSPackages)
admin.site.register(QueryRecord)
