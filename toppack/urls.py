from django.urls import path
from toppack.views import GithubSearchByKeyword, ImportRepository, TopTenJSPackage, JSPackageSearch

urlpatterns = [
    path('search/', GithubSearchByKeyword.as_view()),
    path('import/', ImportRepository.as_view()),
    path('top/', TopTenJSPackage.as_view()),
    path('jspackage/search/', JSPackageSearch.as_view())
]
