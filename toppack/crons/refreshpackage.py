from generic.logs.loggers import Logger
from toppack.github.repository import GetRepositoryDetails, GetFileContent

from generic.db.toppack.package import PackageModelQueries
from generic.db.toppack.jspackage import JSPackageModelQueries
from toppack.serializers import PackageSerializer

error_logs = Logger.refresh_logger()


class PackageRefresher(object):

    @staticmethod
    def get_and_refresh(owner, repo):
        rep_bool, details = GetRepositoryDetails.get_details(owner, repo)
        if not rep_bool:
            print(details)
            return False

        obj = PackageModelQueries.get_by_id(details['id'])
        if not obj:
            details['github_id'] = details['id']
            serializer = PackageSerializer(data=details)
            if not serializer.is_valid():
                print(serializer.errors)
            obj = serializer.save()
        else:
            serializer = PackageSerializer(instance=obj, data=details, partial=True)
            if not serializer.is_valid():
                print(serializer.errors)
                pass
            else:
                obj = serializer.save()

        file_bool, file_content = GetFileContent.get_content(owner, repo)
        if not file_bool:
            print(file_content)
            return False

        packages = []
        package_to_iterate = dict()

        if 'devDependencies' in file_content:
            package_to_iterate = file_content["devDependencies"]
        if 'dependencies' in file_content:
            package_to_iterate = {**package_to_iterate, **(file_content['dependencies'])}

        for package in package_to_iterate:
            package_obj = JSPackageModelQueries.get_by_name(package)
            if not package_obj:
                package_obj = JSPackageModelQueries.create_package(package)
                if not package_obj:
                    continue
            packages.append(package_obj)

        obj.packages.set(packages)
        return True

    def start(self):
        print("starting the package syncing")
        total = 0

        all_packages = PackageModelQueries.get_details_for_refresh()
        for packages in all_packages:
            if not self.get_and_refresh(packages[2]['login'], packages[3]):
                error_logs.error("Failed to sync {}".format(packages[3]))
                print("Failed to sync the package {}/{}".format(packages[2]['login'], packages[3]))
            else:
                print("synced package {}/{}".format(packages[2]['login'], packages[3]))
            total += 1

        print("synced {} packages".format(total))
