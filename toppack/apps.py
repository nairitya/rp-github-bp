from django.apps import AppConfig


class ToppackConfig(AppConfig):
    name = 'toppack'
