from django.db import models
from django.contrib.postgres.fields import JSONField

from django.utils import timezone


# Create your models here.
class Package(models.Model):

    github_id = models.IntegerField(unique=True)
    node_id = models.CharField(max_length=200, null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    full_name = models.CharField(max_length=200, null=True, blank=True)
    private = models.BooleanField(null=True, blank=True)
    owner = JSONField(null=True, blank=True)
    html_url = models.CharField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    fork = models.BooleanField(null=True, blank=True)
    url = models.CharField(max_length=200, null=True, blank=True)
    forks_url = models.CharField(max_length=200, null=True, blank=True)
    keys_url = models.CharField(max_length=200, null=True, blank=True)
    collaborators_url = models.CharField(max_length=200, null=True, blank=True)
    teams_url = models.CharField(max_length=200, null=True, blank=True)
    hooks_url = models.CharField(max_length=200, null=True, blank=True)
    issue_events_url = models.CharField(max_length=200, null=True, blank=True)
    events_url = models.CharField(max_length=200, null=True, blank=True)
    assignees_url = models.CharField(max_length=200, null=True, blank=True)
    branches_url = models.CharField(max_length=200, null=True, blank=True)
    tags_url = models.CharField(max_length=200, null=True, blank=True)
    blobs_url = models.CharField(max_length=200, null=True, blank=True)
    git_tags_url = models.CharField(max_length=200, null=True, blank=True)
    git_refs_url = models.CharField(max_length=200, null=True, blank=True)
    trees_url = models.CharField(max_length=200, null=True, blank=True)
    statuses_url = models.CharField(max_length=200, null=True, blank=True)
    languages_url = models.CharField(max_length=200, null=True, blank=True)
    stargazers_url = models.CharField(max_length=200, null=True, blank=True)
    contributors_url = models.CharField(max_length=200, null=True, blank=True)
    subscribers_url = models.CharField(max_length=200, null=True, blank=True)
    subscription_url = models.CharField(max_length=200, null=True, blank=True)
    commits_url = models.CharField(max_length=200, null=True, blank=True)
    git_commits_url = models.CharField(max_length=200, null=True, blank=True)
    comments_url = models.CharField(max_length=200, null=True, blank=True)
    issue_comment_url = models.CharField(max_length=200, null=True, blank=True)
    contents_url = models.CharField(max_length=200, null=True, blank=True)
    compare_url = models.CharField(max_length=200, null=True, blank=True)
    merges_url = models.CharField(max_length=200, null=True, blank=True)
    archive_url = models.CharField(max_length=200, null=True, blank=True)
    downloads_url = models.CharField(max_length=200, null=True, blank=True)
    issues_url = models.CharField(max_length=200, null=True, blank=True)
    pulls_url = models.CharField(max_length=200, null=True, blank=True)
    milestones_url = models.CharField(max_length=200, null=True, blank=True)
    notifications_url = models.CharField(max_length=200, null=True, blank=True)
    labels_url = models.CharField(max_length=200, null=True, blank=True)
    releases_url = models.CharField(max_length=200, null=True, blank=True)
    deployments_url = models.CharField(max_length=200, null=True, blank=True)
    created_at = models.CharField(max_length=200, null=True, blank=True)
    updated_at = models.CharField(max_length=200, null=True, blank=True)
    pushed_at = models.CharField(max_length=200, null=True, blank=True)
    git_url = models.CharField(max_length=200, null=True, blank=True)
    ssh_url = models.CharField(max_length=200, null=True, blank=True)
    clone_url = models.CharField(max_length=200, null=True, blank=True)
    svn_url = models.CharField(max_length=200, null=True, blank=True)
    homepage = models.CharField(max_length=200, null=True, blank=True)
    size = models.IntegerField(null=True, blank=True)
    stargazers_count = models.CharField(max_length=200, null=True, blank=True)
    watchers_count = models.CharField(max_length=200, null=True, blank=True)
    language = models.CharField(max_length=200, null=True, blank=True)

    has_issues = models.BooleanField(null=True, blank=True)
    has_projects = models.BooleanField(null=True, blank=True)
    has_downloads = models.BooleanField(null=True, blank=True)
    has_wiki = models.BooleanField(null=True, blank=True)
    has_pages = models.BooleanField(null=True, blank=True)

    forks_count = models.IntegerField(null=True, blank=True)
    mirror_url = models.CharField(max_length=200, null=True, blank=True)
    archived = models.BooleanField(null=True, blank=True)
    disabled = models.BooleanField(null=True, blank=True)
    open_issues_count = models.IntegerField(null=True, blank=True)
    forks = models.IntegerField(null=True, blank=True)
    open_issues = models.IntegerField(null=True, blank=True)
    watchers = models.IntegerField(null=True, blank=True)
    default_branch = models.CharField(max_length=200, null=True, blank=True)
    score = models.FloatField(null=True, blank=True)

    created_timestamp = models.DateTimeField(default=timezone.now)

    packages = models.ManyToManyField("toppack.JSPackages", blank=True, null=True)

    def __str__(self):
        return u'{}/{}'.format(self.owner['login'], self.name)

    class Meta:
        app_label = "toppack"


class JSPackages(models.Model):

    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return u'{}'.format(self.name)

    class Meta:
        app_label = "toppack"


class QueryRecord(models.Model):
    query = models.CharField(unique=True, max_length=200)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return u'{}'.format(self.query)

    class Meta:
        app_label = "toppack"


class MiddleTable(models.Model):
    jspackages = models.ForeignKey("JSPackages", on_delete=models.CASCADE)
    package = models.ForeignKey("Package", on_delete=models.CASCADE)

    class Meta:
        db_table = 'toppack_package_packages'
        managed = False
        app_label = "toppack"
